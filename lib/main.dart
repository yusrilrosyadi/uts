import 'package:flutter/material.dart';
import './textoutput.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'TUGAS UTS PEMROGRAMAN MOBILE',
      home: Scaffold(
        backgroundColor: Colors.cyan[100],
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          leading: new Icon(Icons.home),
          title: Text('TUGAS UTS PEMROGRAMAN MOBILE '),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: TextOutput(),
      ),
    );
  }
}